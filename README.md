# gatsby-source-wordpress-experimental

This plugin is a pre-release experimental version of the upcoming gatsby-source-wordpress`V4. It is rewritten from the ground up using WPGraphQL for data sourcing as well as a custom plugin WPGatsby to transform the WPGraphQL schema in Gatsby-specific ways (see`packages/wp-gatsby`).

For now it's not recommended to try installing this plugin as it isn't yet ready for public use.
